Feature: Get request of stats endpoint
  Scenario Outline: Validate get stat method is successfully returning the stats
    Given Port id "<id>" already set in the environment variable
    When I get stats request
    Then I should receive a status code 200 OK
    And I should receive total requests and average time


    Examples:
      |id|
      |8088|