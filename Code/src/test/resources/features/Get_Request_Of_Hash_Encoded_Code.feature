Feature: Get request of hash encoded endpoint
  Scenario Outline: Validate post request of hash endpoint is successfully returning job identifier
    Given Port id "<id>" already set in the environment variable
    When I get encrypted password of a valid job identifier
    Then I should receive a status code 200 OK
    And I should receive a response with encoded password

    Examples:
      |id |
      |8088|
