Feature: Post request of hash endpoint
  Scenario Outline: Validate that the application is returning a job identifier when use POST /hash API endpoint
    Given Port id "<id>" already set in the environment variable
    When I post hash request for the "<password>" password
    Then I should receive a status code 200 OK
    And I should receive a valid job identifier

    Examples:
      |id                                 |password
      |8088                               |Test@12345