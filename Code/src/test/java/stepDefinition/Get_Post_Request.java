package stepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;

import static io.restassured.RestAssured.given;

public class Get_Post_Request {

    RequestSpecification request;
    Response response;
    Headers headers;
    @Given("Port id {string} already set in the environment variable")
    public void setting_port_id(String idVal) {

        RestAssured.baseURI = "http://127.0.0.1:"+idVal;
        headers = new Headers(new Header("Content-Type", "application/json"),
                new Header("Accept", "application/json"));
    }

    @When("I post hash request for the {string} password")
    public void post_hash_request(String passwordValue) {
        JSONObject request = new JSONObject();
        request.put("password", "Test@123458");
        response = given()
                .headers(headers)
                .body(request)
                .when()
                .post("/hash")
                .then()
                .extract()
                .response();
    }

    @Then("I should receive a status code 200 OK")
    public void status_code() {
        System.out.println("Status code: " + response.getStatusLine());
        Assert.assertEquals(response.getStatusCode(), 200);


    }

    @And("I should receive a valid job identifier")
    public void valid_job_identifier() {
        System.out.println("Response Body: " + response.asString());
        Assert.assertTrue(response.asString().matches("\\d+"));
    }

    @When("I get stats request")
    public void get_stats() {
        response = given()
                .headers(headers)
                .when()
                .get("/stats")
                .then()
                .extract()
                .response();
    }

    @And("I should receive total requests and average time")
    public void valid_time() {
        System.out.println(response.asString());
        JsonPath jsonPath = response.jsonPath();
        Assert.assertTrue(jsonPath.get("TotalRequests").toString().matches("\\d+"));
        Assert.assertTrue(jsonPath.get("AverageTime").toString().matches("\\d+"));
        int totalRequests = jsonPath.get("TotalRequests");
        int averageTime = jsonPath.get("AverageTime");
        if(totalRequests > 0){
            Assert.assertTrue(averageTime > 0);
        }
    }

    @When("I get encrypted password of a valid job identifier")
    public void get_encrypted_password() {
        response = given()
                .headers(headers)
                .when()
                .get("/hash/1")
                .then()
                .extract()
                .response();
    }

    @And("I should receive a response with encoded password")
    public void encoded_password() {
        System.out.println(response.asString());
        Assert.assertTrue(response.asString().matches(".+?"));
    }
}
