# JumpCloud

## Getting started
* Code folder contains the JumpCloud automation code
	-> Need IntelliJ to run this code
	-> Java 8 JDK
	-> All Maven dependencies will resolve auto.
	-> From the TestRunner file run the TestRunner class
	-> Three scenarios will trigger written in cucumber

* Documentation folder contains the Test Execution Report


